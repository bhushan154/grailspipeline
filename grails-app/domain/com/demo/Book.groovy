package com.demo

class Book {

    static belongsTo = [author:Author]

    String title
    String isbn

    static constraints = {
    }
}
