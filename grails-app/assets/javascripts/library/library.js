//= wrapped
//= require /angular/angular
//= require /library/core/library.core
//= require /library/index/library.index

angular.module("library", [
    "library.core",
    "library.index"
]);
