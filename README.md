# Bitbucket Pipelines #

This is a demo project to explore the latest feature on Bitbucket Cloud called Pipelines.

### What is this repository for? ###

* How to create a Grails project
* How to generate REST controllers in a Grail project
* [Bitbucket Pipelines](https://confluence.atlassian.com/pages/viewpage.action?pageId=792496469)
* How to setup Bitbucket Pipelines for a Grails project

### How do I get set up? ###

* Environment

Ubuntu with bzip2 and libfontconfig
Grails 3.1.7

* Dependencies

Karma needs the following on Ubuntu environments

apt-get install bzip2
apt-get install libfontconfig

* How to run tests

grails test-app -unit

* Deployment instructions

grails war

### Can I contribute? ###

Please feel free to create a pull request with a good description of what you are contributing.

### Who do I talk to? ###

Create an issue on this repository. Contact bhushan154